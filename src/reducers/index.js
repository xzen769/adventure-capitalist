import { combineReducers } from 'redux';

import data from './data';
import barrePrice from '../components/barre-price/reducer';

export default combineReducers({
  data,
  barrePrice
});
