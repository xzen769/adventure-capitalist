import React from 'react';
import { connect } from 'react-redux';
import { Container, Row } from 'react-bootstrap';

import Product from '../product';
import BarrePrice from '../barre-price';
import NewProduct from '../new-product';
import ManagersList from '../managers-list';

const Home = ({ data }) => (
  <Container className="bg-dark" style={{ height: '100%' }}>
    <BarrePrice />
    <ManagersList />
    <Row>
      {data
        .filter((item) => item.unlock)
        .map((item) => <Product key="test" item={item} />)}
    </Row>
    <NewProduct />
  </Container>
);

const mapToProps = (state) => ({
  data: state.data
});

export default connect(mapToProps)(Home);
